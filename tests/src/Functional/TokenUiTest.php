<?php

namespace Drupal\Tests\custom_tokens\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * The token UI test.
 *
 * @group custom_tokens
 */
class TokenUiTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'custom_tokens',
    'block',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->drupalLogin($this->createUser([
      'administer custom tokens',
      'access administration pages',
    ]));
    $this->drupalPlaceBlock('local_actions_block');
  }

  /**
   * Test creating token entities from the UI.
   */
  public function testTokenUi() {
    $this->drupalGet('admin/structure');
    $this->clickLink('Custom Tokens');
    $this->clickLink('Add Token');

    $this->submitForm([
      'label' => 'Foo',
      'id' => 'foo',
      'tokenName' => 'Bar',
      'tokenValue' => 'Baz',
    ], 'Save');

    $assert = $this->assertSession();
    $assert->pageTextContains('Custom token was successfully saved.');

    $assert->elementContains('css', 'table', 'Foo');
    $assert->elementContains('css', 'table', '[Bar]');
    $assert->elementContains('css', 'table', 'Baz');

    $this->clickLink('Edit');

    $assert->fieldValueEquals('label', 'Foo');
    $assert->fieldValueEquals('id', 'foo');
    $assert->fieldValueEquals('tokenName', 'Bar');
    $assert->fieldValueEquals('tokenValue', 'Baz');

    $this->clickLink('Delete');
    $this->submitForm([], 'Delete');

    $assert->pageTextContains('The token Foo has been deleted.');
    $this->addToAssertionCount(1);
  }

}
